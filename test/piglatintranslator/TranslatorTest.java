package piglatintranslator;
import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testinputPhrase()
	{
		String inputPhrase= "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world",translator.getPhrase());
	}
	
	
	@Test
	public void testTranslationEmptyphrase()
	{
		String inputPhrase=" ";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL,translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVocalEndingwhithy()
	{
		String inputPhrase= "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingwhithy()
	{
		String inputPhrase= "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithvowelEndingwhithvowel()
	{
		String inputPhrase= "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithvowelEndingwhithconsonant()
	{
		String inputPhrase= "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay",translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithsigleconsonant()
	{
		String inputPhrase= "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithsigleconsonantycase()
	{
		String inputPhrase= "yellow";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellowyay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithmoreconsonant()
	{
		String inputPhrase= "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay",translator.translate());
	}
	
	
	@Test
	public void testTranslationPhrasesStartingWithmoreword()
	{
		String inputPhrase= "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway",translator.translate());
	}
	
	@Test
	public void testTranslationPhrasesStartingWithmorewordwithseparator()
	{
		String inputPhrase= "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay",translator.translate());
	}
   
	@Test
	public void testTranslationPhrasesStartingWithmorewordwithpunctuation()
	{
		String inputPhrase= "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!",translator.translate());
	}
}
