package piglatintranslator;
import java.util.StringTokenizer;

public class Translator {

	public static final String NIL = "nil";
	
	private String phrase;

	private int[] punctuations;
	
	
	public Translator(String inputPhrase) {
		phrase=inputPhrase;
	}

	public String getPhrase() {
		
		return phrase;
	}

	public String translate() {
		
		 punctuations = new int [phrase.length()+1];
			String[] words={};
		int i;
		
			
		if(phrase.contains(".")||phrase.contains(",")||phrase.contains(";")||phrase.contains(":")||phrase.contains("?")||phrase.contains("!")||phrase.contains("'")||phrase.contains("(")||phrase.contains(")"))
		{
			return phrase=translatewithpuntuation(words);	
		}
	

		
		if(phrase.contains("-")|| phrase.length() - phrase.replaceAll(" ", "").length()>=1 && phrase!=(" ") )
		{
		   return phrase=translatemorewords(words);
		}
		
		
		
		  if(startWithVowel())
          {
        	  
        	  if(endingWithVowel())
        	  {
        		 return phrase + "yay";
        	   }
        	  else
        	  { 
        		  if(phrase.endsWith("y") ) {
        	  
	                return phrase + "nay";
                  }
        		  else
        		  {
             	         return phrase + "ay";
                          	    
        		  }
        		 
        	  }
          }
          else {
          
        	   if(startWithConsonant())
              { 
        		   
        		  
        		   String subphrase=phrase.substring(1);
        		   if(startWithmoreconsonant(subphrase))
                   { 
        			  
        			 char sub3=phrase.charAt(0);
        			
        		
                  	 String phrase1=phrase.replace(sub3,' ') + sub3;
                  	 String phrase2=phrase1.trim(); 
                  	 String phrase3=phrase2.replaceFirst("n"," ") + phrase.charAt(1) + "ay";
                  	 return phrase3.trim();
                   }
                   if(thereisavowel(phrase.substring(1)))
                   {  
                	 char sub1=phrase.charAt(0);
                	 String phrase1=phrase.replace(phrase.charAt(0),' ') + sub1 + "ay";
                	 return phrase1.trim();
                   }
           		
              }
        	   
        	   
          }
		
		return NIL;  
	}
	

	
	private boolean startWithVowelar(String words[],int i)
	{
		

		return words[i].startsWith("a") || words[i].startsWith("e")  ||  words[i].startsWith("i") ||  words[i].startsWith("o") ||  words[i].startsWith("u");
		
		
	}
	
	
	private boolean startWithConsonantar(String words[],int i)
	{
		

		 return !words[i].startsWith("a") && !words[i].startsWith("e")  && !words[i].startsWith("i") && !words[i].startsWith("o") && !words[i].startsWith("u");
	
		
	}
	
	private boolean endingWithVowelar(String words[],int i) 
	{

		return words[i].endsWith("a") || words[i].endsWith("e")  || words[i].endsWith("i") || words[i].endsWith("o") || words[i].endsWith("u");
		
	}
	
	private boolean startWithVowel() 
	{

		return phrase.startsWith("a") || phrase.startsWith("e")  || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u");
		
			
	}
	
	private boolean endingWithVowel() 
	{

		return phrase.endsWith("a") || phrase.endsWith("e")  || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u");
		
	}
	
	
	private boolean thereisavowel(String sub) 
	{
	 return sub.startsWith("a") || sub.startsWith("e")  || sub.startsWith("i") || sub.startsWith("o") || sub.startsWith("u");	 
	}
	
	
	private boolean startWithmoreconsonant(String sub)
	{
		
		 return !sub.startsWith("a") && !sub.startsWith("e")  && !sub.startsWith("i") && !sub.startsWith("o") && !sub.startsWith("u");
	
	}
	
	private boolean startWithConsonant()
	{
		
		 return !phrase.startsWith("a") && !phrase.startsWith("e")  && !phrase.startsWith("i") && !phrase.startsWith("o") && !phrase.startsWith("u") && !phrase.startsWith(" ");
	
	}
	
	private String translatewithpuntuation(String[] words)
	{
		String phrase5 = null;
		char point = 0;
		int dimension=phrase.length()+1;
		punctuations = new int [phrase.length()+1];
		
		
		for(int i=1;i!=phrase.length()+1;i++)
		{
			punctuations[i]=10;
		}
		
		
			if(phrase.contains("."))
			{
				punctuations[1]=1;
				phrase5=phrase.replace(".","");
			}
			else
			{
				if(phrase.contains(","))
				{
					punctuations[2]=1;
					phrase5=phrase.replace(",","");
				}
				else
				{
					if(phrase.contains(";"))
					{
						punctuations[3]=1;
						phrase5=phrase.replace(";","");
					}
					else
					{
						if(phrase.contains(":"))
						{
							punctuations[4]=1;
							phrase5=phrase.replace(":","");
						}
						else
						{
							if(phrase.contains("?"))
							{
								punctuations[5]=1;
								phrase5=phrase.replace("?","");
							}
							else
							{
								if(phrase.contains("!"))
								{
									punctuations[6]=1;
								   phrase5=phrase.replace("!","");
									
								}
								else
								{
									if(phrase.contains("'"))
									{
										punctuations[7]=1;
										phrase5=phrase.replace("'","");
									}
									else 
									{
										if(phrase.contains("("))
										{
											punctuations[8]=1;
											phrase5=phrase.replace("(","");
										}
										else
										{
											if(phrase.contains(")"))
											{
												punctuations[9]=1;
												phrase5=phrase.replace(")","");
											}
											
										}
								       
									}
								}
							}
						}
						
					}
				}
			}
			
		
		
		

		
		if(phrase.contains("-")|| phrase.length() - phrase.replaceAll(" ", "").length()>=1 && phrase!=(" ") )
		{
			
			if(phrase.contains("-"))
			{
			 words = phrase5.split("-");
			}else
			{ 
				words= phrase5.split("\\s+");
			}
			
		for ( int i=0;i!=words.length;i++)
		{
			
			
          if(startWithVowelar(words,i))
          {
        	  
        	  if(endingWithVowelar(words,i))
        	  {
        		 words[i]=words[i]+ "yay";
        	   }
        	  else
        	  { 
        		  if(phrase.endsWith("y") ) {
        	  
	                words[i]=words[i] + "nay";
                  }
        		  else
        		  {
             	        words[i]=words[i] + "ay";
                          	    
        		  }
        		 
        	  }
          }
          else {
          
        	   if(startWithConsonantar(words,i))
              { 
        		   
        		   
                   if(thereisavowel(phrase.substring(1)))
                   {  
                	 char sub1=words[i].charAt(0);
                	 String phrase1=words[i].replace(words[i].charAt(0),' ') + sub1 + "ay";
                	 words[i]=phrase1.trim();
                	 
                   }
           		
              }
        	   
        	   
          }
          
		}
		if(phrase.contains("-"))
		{
		phrase="\0";
		phrase=phrase.trim();
		
		for (int i=0;i!=words.length;i++)
		{
			phrase=phrase+"-"+words[i];
			
		}

		if(punctuations[1]==1)
		{
			
			phrase=phrase+".";
		}
		else
		{
			if(punctuations[2]==1)
			{
				phrase=phrase+",";
				
			}
			else
			{
				if(punctuations[3]==1)
				{
					
					phrase=phrase+";";
				}
				else
				{
					if(punctuations[4]==1)
					{
						
						phrase=phrase+":";
					}
					else
					{
						if(punctuations[5]==1)
						{
							
							phrase=phrase+"?";
						}
						else
						{
							if(punctuations[6]==1)
							{
								
								phrase=phrase+"!";
								
							}
							else
							{
								if(punctuations[7]==1)
								{
									
									phrase=phrase+"'";
								}
								else 
								{
									if(punctuations[8]==1)
									{
										
										phrase=phrase+"(";
									}
									else
									{
										if(punctuations[9]==1)
										{
											
											phrase=phrase+")";
										}
										
									}
							       
								}
							}
						}
					}
					
				}
			}
		}
		 phrase=phrase.replaceFirst("-","");
		}
		if(phrase.contains(" "))
		{
		
		phrase="\0";
		phrase=phrase.trim();
		
		for (int i=0;i!=words.length;i++)
		{
			phrase=phrase+" "+words[i];
		}
		
		
			
			if(punctuations[1]==1)
			{
				
				phrase=phrase+".";
			}
			else
			{
				if(punctuations[2]==1)
				{
					phrase=phrase+",";
					
				}
				else
				{
					if(punctuations[3]==1)
					{
						
						phrase=phrase+";";
					}
					else
					{
						if(punctuations[4]==1)
						{
							
							phrase=phrase+":";
						}
						else
						{
							if(punctuations[5]==1)
							{
								
								phrase=phrase+"?";
							}
							else
							{
								if(punctuations[6]==1)
								{
									
									phrase=phrase+"!";
									
								}
								else
								{
									if(punctuations[7]==1)
									{
										
										phrase=phrase+"'";
									}
									else 
									{
										if(punctuations[8]==1)
										{
											
											phrase=phrase+"(";
										}
										else
										{
											if(punctuations[9]==1)
											{
												
												phrase=phrase+")";
											}
											
										}
								       
									}
								}
							}
						}
						
					}
				}
			}
		
		
		
		 phrase=phrase.replaceFirst(" ","");
		}
		 	
		}
		
		
		return phrase;
					
	}
	
	private String translatemorewords(String[] words)
	{
		
		
		if(phrase.contains("-"))
		{
		 words = phrase.split("-");
		}else
		{ 
			words= phrase.split("\\s+");
		}
		
	for (int i=0;i!=words.length;i++)
	{
		
		
      if(startWithVowelar(words,i))
      {
    	  
    	  if(endingWithVowelar(words,i))
    	  {
    		 words[i]=words[i]+ "yay";
    	   }
    	  else
    	  { 
    		  if(phrase.endsWith("y") ) {
    	  
                words[i]=words[i] + "nay";
              }
    		  else
    		  {
         	        words[i]=words[i] + "ay";
                      	    
    		  }
    		 
    	  }
      }
      else {
      
    	   if(startWithConsonantar(words,i))
          { 
    		   
    		   
               if(thereisavowel(phrase.substring(1)))
               {  
            	 char sub1=words[i].charAt(0);
            	 String phrase1=words[i].replace(words[i].charAt(0),' ') + sub1 + "ay";
            	 words[i]=phrase1.trim();
            	 
               }
       		
          }
    	   
    	   
      }
      
	}
	if(phrase.contains("-"))
	{
	phrase="\0";
	phrase=phrase.trim();
	
	for (int i=0;i!=words.length;i++)
	{
		phrase=phrase+"-"+words[i];
		
	}
	 phrase=phrase.replaceFirst("-","");
	}
	if(phrase.contains(" "))
	{
	phrase="\0";
	phrase=phrase.trim();
	
	for (int i=0;i!=words.length;i++)
	{
		phrase=phrase+" "+words[i];
		
	}
	 phrase=phrase.replaceFirst(" ","");
	}
	 return phrase;	
	}
	
	
}
